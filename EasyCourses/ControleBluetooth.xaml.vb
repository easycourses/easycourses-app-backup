﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Net
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Navigation
Imports Microsoft.Phone.Controls
Imports Microsoft.Phone.Shell
Imports Windows.Networking.Sockets
Imports Windows.Networking.Proximity
Imports System.Diagnostics
Imports Windows.Storage.Streams
Imports System.Threading.Tasks
Imports System.Windows.Media

Partial Public Class ControleBluetooth
    Inherits PhoneApplicationPage
    Private connectionManager As ConnectionManager

    Private stateManager As StateManager

    ' Constructor
    Public Sub New()
        InitializeComponent()
        connectionManager = New ConnectionManager()
        AddHandler connectionManager.MessageReceived, AddressOf connectionManager_MessageReceived

        stateManager = New StateManager()
    End Sub

    Private Async Function connectionManager_MessageReceived(message As String) As Task
        Debug.WriteLine(Convert.ToString("Message received:") & message)
        Dim messageArray As String() = message.Split(":"c)
        Select Case messageArray(0)
            Case "LED_RED"
                stateManager.RedLightOn = If(messageArray(1) = "ON", True, False)
                Dispatcher.BeginInvoke(Sub() RedButton.Background = If(stateManager.RedLightOn, New SolidColorBrush(Colors.Red), New SolidColorBrush(Colors.Black)))
                Exit Select
            Case "LED_GREEN"
                stateManager.GreenLightOn = If(messageArray(1) = "ON", True, False)
                Dispatcher.BeginInvoke(Sub() GreenButton.Background = If(stateManager.GreenLightOn, New SolidColorBrush(Colors.Green), New SolidColorBrush(Colors.Black)))
                Exit Select
            Case "LED_YELLOW"
                stateManager.YellowLightOn = If(messageArray(1) = "ON", True, False)
                Dispatcher.BeginInvoke(Sub() YellowButton.Background = If(stateManager.YellowLightOn, New SolidColorBrush(Colors.Yellow), New SolidColorBrush(Colors.Black)))
                Exit Select
            Case "PROXIMITY"
                stateManager.BodyDetected = If(messageArray(1) = "DETECTED", True, False)
                If stateManager.BodyDetected Then
                    Dispatcher.BeginInvoke(Sub()
                                               BodyDetectionStatus.Text = "Intruder detected!!!"
                                               BodyDetectionStatus.Foreground = New SolidColorBrush(Colors.Red)

                                           End Sub)
                    Await connectionManager.SendCommand("TURN_ON_RED")
                Else
                    Dispatcher.BeginInvoke(Sub()

                                               BodyDetectionStatus.Text = "No body detected"
                                               BodyDetectionStatus.Foreground = New SolidColorBrush(Colors.White)

                                           End Sub)
                End If
                Exit Select
        End Select
    End Function

    Protected Overrides Sub OnNavigatedTo(e As NavigationEventArgs)
        connectionManager.Initialize()
        stateManager.Initialize()
    End Sub

    Protected Overrides Sub OnNavigatedFrom(e As NavigationEventArgs)
        connectionManager.Terminate()
    End Sub

    Private Sub ConnectAppToDeviceButton_Click_1(sender As Object, e As RoutedEventArgs)
        AppToDevice()
    End Sub

    Private Async Function AppToDevice() As Task
        ConnectAppToDeviceButton.Content = "Connecting..."
        PeerFinder.AlternateIdentities("Bluetooth:Paired") = ""
        Dim pairedDevices = Await PeerFinder.FindAllPeersAsync()

        If pairedDevices.Count = 0 Then
            Debug.WriteLine("No paired devices were found.")
        Else
            For Each pairedDevice In pairedDevices
                If pairedDevice.DisplayName = DeviceName.Text Then
                    Await connectionManager.Connect(pairedDevice.HostName)
                    ConnectAppToDeviceButton.Content = "Connected"
                    DeviceName.IsReadOnly = True
                    ConnectAppToDeviceButton.IsEnabled = False
                    Continue For
                End If
            Next
        End If
    End Function

    Private Async Sub RedButton_Click_1(sender As Object, e As RoutedEventArgs)
        Dim command As String = If(stateManager.RedLightOn, "TURN_OFF_RED", "TURN_ON_RED")
        Await connectionManager.SendCommand(command)
    End Sub

    Private Async Sub GreenButton_Click_1(sender As Object, e As RoutedEventArgs)
        Dim command As String = If(stateManager.GreenLightOn, "TURN_OFF_GREEN", "TURN_ON_GREEN")
        Await connectionManager.SendCommand(command)
    End Sub

    Private Async Sub YellowButton_Click_1(sender As Object, e As RoutedEventArgs)
        Dim command As String = If(stateManager.YellowLightOn, "TURN_OFF_YELLOW", "TURN_ON_YELLOW")
        Await connectionManager.SendCommand(command)
    End Sub
End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
