﻿Imports System.IO.IsolatedStorage

Module PublicThings
    Public ListeObjetsCourses As New List(Of ElementListeDeCourses)

    Public Sub ChargerListeCourses()
        ListeObjetsCourses = New List(Of ElementListeDeCourses)
        Dim ContenuIsolatedStorage As String
        Try
            ContenuIsolatedStorage = System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings("courses")
            'MessageBox.Show(ContenuIsolatedStorage)
        Catch ex As Exception
            EcrireSetting("courses", "")
            ChargerListeCourses()
            Exit Sub
        End Try
        Dim TableauSplitSetting() As String = ContenuIsolatedStorage.Split(";")
        If TableauSplitSetting.Count > 1 And TableauSplitSetting(0) <> "" Then
            For i = 0 To TableauSplitSetting.Count - 1
                Dim TableauSplitThisElement() As String = TableauSplitSetting(i).Split(",")
                ListeObjetsCourses.Add(New ElementListeDeCourses(TableauSplitThisElement(1), TableauSplitThisElement(0), TableauSplitThisElement(2)))
            Next
        End If
    End Sub
    Public Sub SauvegarderListeCourses()
        Dim StringAEcrire As String = ""
        For i = 0 To ListeObjetsCourses.Count - 1
            Dim PointVirgule As String = ""
            If i <> ListeObjetsCourses.Count - 1 Then
                PointVirgule = ";"
            End If
            StringAEcrire &= ListeObjetsCourses(i).ID & "," & ListeObjetsCourses(i).Contenu & "," & ListeObjetsCourses(i).Completed & PointVirgule
        Next
        IsolatedStorageSettings.ApplicationSettings("courses") = StringAEcrire
        IsolatedStorageSettings.ApplicationSettings.Save()
    End Sub
    Public Sub EcrireSetting(Setting As String, Content As String)
        Try
            IsolatedStorageSettings.ApplicationSettings.Add(Setting, Content)
        Catch ex As Exception
            IsolatedStorageSettings.ApplicationSettings(Setting) = Content
        End Try
    End Sub
    Public Function getLastID() As Integer
        Dim Resultat As Integer = 0
        If ListeObjetsCourses.Count > 0 Then
            For i = 0 To ListeObjetsCourses.Count - 1
                If ListeObjetsCourses(i).ID > Resultat Then
                    Resultat = ListeObjetsCourses(i).ID
                End If
            Next
        Else
            Resultat = -1
        End If
        Return Resultat
    End Function
End Module
