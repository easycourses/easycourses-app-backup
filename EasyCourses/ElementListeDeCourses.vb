﻿Public Class ElementListeDeCourses
    Public Property Contenu As String
    Public Property ID As Integer
    Public Property Completed As Boolean

    Public Sub New(Contenu As String, ID As Integer, Completed As Boolean)
        Me.Contenu = Contenu
        Me.ID = ID
        Me.Completed = Completed
    End Sub
    Public Sub Complete()
        Me.Completed = True
    End Sub

    Public Sub Uncomplete()
        Me.Completed = False
    End Sub
End Class
