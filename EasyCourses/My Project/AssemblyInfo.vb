﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Resources

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly
<Assembly: AssemblyTitle("EasyCourses")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyConfiguration("")> 
<Assembly: AssemblyCompany("Trinitrooxypropane Dev Team")> 
<Assembly: AssemblyProduct("EasyCourses")> 
<Assembly: AssemblyCopyright("Copyright © Thomas Kowalski 2014")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: AssemblyCulture("")> 

' L'affectation de la valeur false à ComVisible rend les types invisibles dans cet assembly 
' aux composants COM.  Si vous devez accéder à un type dans cet assembly à partir de 
' COM, affectez la valeur true à l'attribut ComVisible sur ce type.
<assembly: ComVisible(false)>

' Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<assembly: Guid("46affba4-f576-4268-a8f9-21dfe5cc65a0")>

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de révision et de build par défaut 
' en utilisant '*', comme indiqué ci-dessous :
<assembly: AssemblyVersion("1.0.0.0")>
<assembly: AssemblyFileVersion("1.0.0.0")>
<assembly: NeutralResourcesLanguageAttribute("fr-FR")>
