﻿Partial Public Class NouvelElementListeDeCourses
    Inherits PhoneApplicationPage

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub NouvelElementListeDeCourses_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        ApplicationBar = New ApplicationBar()
        ApplicationBar.Mode = ApplicationBarMode.Default
        ApplicationBar.Opacity = 1.0
        ApplicationBar.IsVisible = True
        ApplicationBar.IsMenuEnabled = True
        Dim validerMaggle As ApplicationBarIconButton = New ApplicationBarIconButton()
        validerMaggle.IconUri = New Uri("/Images/done.png", UriKind.Relative)
        validerMaggle.Text = "Valider"
        ApplicationBar.Buttons.Add(validerMaggle)
        AddHandler validerMaggle.Click, AddressOf validerMaggle_Click
    End Sub

    Private Sub validerMaggle_Click()
        ListeObjetsCourses.Add(New ElementListeDeCourses(textboxContenu.Text, getLastID() + 1, False))
        SauvegarderListeCourses()
        NavigationService.Navigate(New Uri("/MainPage.xaml", UriKind.Relative))
    End Sub
End Class
