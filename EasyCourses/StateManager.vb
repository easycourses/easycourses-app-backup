﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks

''' <summary>
''' Class to control the state of the Arduino
''' </summary>
Class StateManager
    ''' <summary>
    ''' The red led state 
    ''' </summary>
    Public Property RedLightOn() As Boolean
        Get
            Return m_RedLightOn
        End Get
        Set(value As Boolean)
            m_RedLightOn = value
        End Set
    End Property
    Private m_RedLightOn As Boolean

    ''' <summary>
    ''' The green led state 
    ''' </summary>
    Public Property GreenLightOn() As Boolean
        Get
            Return m_GreenLightOn
        End Get
        Set(value As Boolean)
            m_GreenLightOn = value
        End Set
    End Property
    Private m_GreenLightOn As Boolean

    ''' <summary>
    ''' The yellow led state 
    ''' </summary>
    Public Property YellowLightOn() As Boolean
        Get
            Return m_YellowLightOn
        End Get
        Set(value As Boolean)
            m_YellowLightOn = value
        End Set
    End Property
    Private m_YellowLightOn As Boolean

    ''' <summary>
    ''' The proximity sensor state.
    ''' </summary>
    Public Property BodyDetected() As Boolean
        Get
            Return m_BodyDetected
        End Get
        Set(value As Boolean)
            m_BodyDetected = value
        End Set
    End Property
    Private m_BodyDetected As Boolean

    ''' <summary>
    ''' Initialize the state manager.
    ''' </summary>
    Public Sub Initialize()
        RedLightOn = False
        GreenLightOn = False
        YellowLightOn = False
        BodyDetected = False
    End Sub
End Class
