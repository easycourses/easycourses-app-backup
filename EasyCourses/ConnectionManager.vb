﻿
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Windows.Networking
Imports Windows.Networking.Sockets
Imports Windows.Storage.Streams

''' <summary>
''' Class to control the bluetooth connection to the Arduino.
''' </summary>
Public Class ConnectionManager
    ''' <summary>
    ''' Socket used to communicate with Arduino.
    ''' </summary>
    Private socket As StreamSocket

    ''' <summary>
    ''' DataWriter used to send commands easily.
    ''' </summary>
    Private dataWriter As DataWriter

    ''' <summary>
    ''' DataReader used to receive messages easily.
    ''' </summary>
    Private dataReader As DataReader

    ''' <summary>
    ''' Thread used to keep reading data from socket.
    ''' </summary>
    Private dataReadWorker As BackgroundWorker

    ''' <summary>
    ''' Delegate used by event handler.
    ''' </summary>
    ''' <param name="message">The message received.</param>
    Public Delegate Sub MessageReceivedHandler(message As String)

    ''' <summary>
    ''' Event fired when a new message is received from Arduino.
    ''' </summary>
    Public Event MessageReceived As MessageReceivedHandler

    ''' <summary>
    ''' Initialize the manager, should be called in OnNavigatedTo of main page.
    ''' </summary>
    Public Sub Initialize()
        socket = New StreamSocket()
        dataReadWorker = New BackgroundWorker()
        dataReadWorker.WorkerSupportsCancellation = True
        AddHandler dataReadWorker.DoWork, AddressOf ReceiveMessages
    End Sub

    ''' <summary>
    ''' Finalize the connection manager, should be called in OnNavigatedFrom of main page.
    ''' </summary>
    Public Sub Terminate()
        If socket IsNot Nothing Then
            socket.Dispose()
        End If
        If dataReadWorker IsNot Nothing Then
            dataReadWorker.CancelAsync()
        End If
    End Sub

    ''' <summary>
    ''' Connect to the given host device.
    ''' </summary>
    ''' <param name="deviceHostName">The host device name.</param>
    Public Async Function Connect(deviceHostName As HostName) As Task
        If socket IsNot Nothing Then
            Await socket.ConnectAsync(deviceHostName, "1")
            dataReader = New DataReader(socket.InputStream)
            dataReadWorker.RunWorkerAsync()
            dataWriter = New DataWriter(socket.OutputStream)
        End If
    End Function

    ''' <summary>
    ''' Receive messages from the Arduino through bluetooth.
    ''' </summary>
    Private Async Sub ReceiveMessages(sender As Object, e As DoWorkEventArgs)
        Try
            While True
                ' Read first byte (length of the subsequent message, 255 or less). 
                Dim sizeFieldCount As UInteger = Await dataReader.LoadAsync(1)
                If sizeFieldCount <> 1 Then
                    ' The underlying socket was closed before we were able to read the whole data. 
                    Return
                End If

                ' Read the message. 
                Dim messageLength As UInteger = dataReader.ReadByte()
                Dim actualMessageLength As UInteger = Await dataReader.LoadAsync(messageLength)
                If messageLength <> actualMessageLength Then
                    ' The underlying socket was closed before we were able to read the whole data. 
                    Return
                End If
                ' Read the message and process it.
                Dim message As String = dataReader.ReadString(actualMessageLength)
                RaiseEvent MessageReceived(message)
            End While
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Send command to the Arduino through bluetooth.
    ''' </summary>
    ''' <param name="command">The sent command.</param>
    ''' <returns>The number of bytes sent</returns>
    Public Async Function SendCommand(command As String) As Task(Of UInteger)
        Dim sentCommandSize As UInteger = 0
        If dataWriter IsNot Nothing Then
            Dim commandSize As UInteger = dataWriter.MeasureString(command)
            dataWriter.WriteByte(CByte(commandSize))
            sentCommandSize = dataWriter.WriteString(command)
            Await dataWriter.StoreAsync()
        End If
        Return sentCommandSize
    End Function
End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
