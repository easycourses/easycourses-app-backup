﻿Imports System
Imports System.Threading
Imports System.Windows.Controls
Imports Microsoft.Phone.Controls
Imports Microsoft.Phone.Shell

Partial Public Class MainPage
    Inherits PhoneApplicationPage

    ' Constructeur
    Public Sub New()
        InitializeComponent()

        ' Affecter l'exemple de données au contexte de données du contrôle ListBox
        DataContext = App.ViewModel
    End Sub

    ' Charger les données pour les éléments ViewModel
    Protected Overrides Sub OnNavigatedTo(e As NavigationEventArgs)
        If Not App.ViewModel.IsDataLoaded Then
            App.ViewModel.LoadData()
        End If
    End Sub

    Private Sub BoutonArriere_Hold(sender As Object, e As GestureEventArgs) Handles BoutonArriere.Hold
        'BoutonArriere.Background = New SolidColorBrush(Colors.White)
    End Sub

    Private Sub panoramaPrincipal_SelectionChanged() Handles panoramaPrincipal.SelectionChanged
        If panoramaPrincipal.SelectedIndex = 0 Then
            ApplicationBar = New ApplicationBar()
            ApplicationBar.Mode = ApplicationBarMode.Default
            ApplicationBar.Opacity = 1.0
            ApplicationBar.IsVisible = True
            ApplicationBar.IsMenuEnabled = True
            Dim nouveau As ApplicationBarIconButton = New ApplicationBarIconButton()
            nouveau.IconUri = New Uri("/Images/new.png", UriKind.Relative)
            nouveau.Text = "Nouveau"
            ApplicationBar.Buttons.Add(nouveau)
            AddHandler nouveau.Click, AddressOf nouveau_Click
        Else
            ApplicationBar = Nothing
        End If
    End Sub
    Private Sub nouveau_Click(ByVal sender As Object, ByVal e As EventArgs)
        NavigationService.Navigate(New Uri("/NouvelElementListeDeCourses.xaml", UriKind.Relative))
    End Sub

    Private Sub MainPage_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        panoramaPrincipal_SelectionChanged()
        ChargerListeCourses()
        listeCourses.ItemsSource = ListeObjetsCourses
        'MessageBox.Show(System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings("courses"))
    End Sub

    Private Sub maggle_Tap(sender As Object, e As GestureEventArgs) Handles maggle.Tap
        For i = 0 To ListeObjetsCourses.Count - 1
            MessageBox.Show(ListeObjetsCourses(i).Contenu)
        Next
    End Sub
End Class